---
title: "Infinity"
description: "Infinity, a scientific insight of the highest order as it postulates the very perpetual extent of time as well of space. There is no center as there is no beginning nor end. Two ancient symbols known throughout the world and ages testify of this wisdom of old given to us by the Elohim, namely the Swastika representing the cyclical nature of time and timelessness, and Magen David representing in turn that what is above is below, meaning the way space expands to greater macrosopic levels is the same as the way the space within the microscopic levels folds down to."
lead: "A scientific insight of the highest order as it postulates the very perpetual extent of time as well of space. There is no center as there is no beginning nor end. Two ancient symbols known throughout the world and ages testify of this wisdom of old given to us by the Elohim, namely the Swastika representing the cyclical nature of time and timelessness, and Magen David representing in turn that what is above is below, meaning the way space expands to greater macrosopic levels is the same as the way the space within the microscopic levels folds down to."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  wiki:
    parent: "Wiki"
weight: 200
toc: true
---

## Infinity in space

The concept of an infinite universe in space is based on the idea that there is no edge or boundary to the universe, and that it extends forever in all directions. This idea is supported by the observed uniformity and homogeneity of the cosmic microwave background radiation, which suggests that the universe is isotropic (the same in all directions) and homogeneous (the same at all points).

Additionally, the fact that we have not yet observed a distinct edge to the universe, despite our increasing technological capabilities and knowledge, also supports the idea of an infinite universe.

### Magen David ✡

## Infinity in time

The concept of infinite time is related to the idea that time has always existed and will always exist, without any beginning or end. This idea is based on the idea that time is a fundamental aspect of the universe and cannot be created or destroyed.

In physics, the concept of time as an infinite and unchanging entity is supported by the laws of thermodynamics, which suggest that time is irreversible and that entropy (a measure of the disorder of a system) always increases over time. This suggests that the universe has a definite history and that time cannot be reversed, supporting the idea of an infinite and unchanging time.

### Swastika ࿕

## See also

[Wiki › Mass effect]({{< relref "wiki/encyclopedia/mass-effect.md" >}})</br>
[Wiki › Raelian Symbol of Infinity]({{< relref "wiki/encyclopedia/raelian-symbol-of-infinity.md" >}})</br>
[Wiki › Star of David]({{< relref "wiki/encyclopedia/star-of-david.md" >}})</br>
[Wiki › Swastika]({{< relref "wiki/encyclopedia/swastika.md" >}})</br>
