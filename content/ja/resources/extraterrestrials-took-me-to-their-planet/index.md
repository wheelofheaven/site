---
title: "Extraterrestrials Took Me To Their Planet"
description: "Claude Vorhilon aka Raël — Extraterrestrials Took Me To Their Planet (1976)"
lead: "The second book by Raël, also a written testimony of another encounter with Yahweh where he was taken to another planet."
author: "Claude Vorhilon (aka Raël)"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://wheelofheaven.github.io/rael-two-extraterrestrials-took-me-to-their-planet/"
menu:
  resources:
    parent: "browse"
weight: 220
toc: false
pinned: true
featured: false
contributors: [""]
types: ["Maintained Books"]
topics: ["Ancient Astronauts", "Canon", "Elohim", "Intelligent Design", "Neo-Euhemerism"]
---

"Extraterrestrials Took Me to Their Planet" is a book written by Claude Vorilhon, also known as Raël, the founder of the Raëlian Movement. The book is a sequel to Raël's first book, "The Book Which Tells the Truth," and it expands on the ideas and teachings presented in that work.

In "Extraterrestrials Took Me to Their Planet," Raël describes his experiences with extraterrestrial beings, whom he refers to as the Elohim. He claims that he was taken by the Elohim to their planet, where he received further teachings and insights about the nature of reality and the purpose of human existence.

The book presents Raël's beliefs about the origins of the universe and of life on Earth, and argues that the Elohim created humanity through genetic engineering. Raël also discusses the evolution of human consciousness and the role of science and technology in that evolution.

In "Extraterrestrials Took Me to Their Planet," Raël also presents his views on spirituality, morality, and ethics, and argues that these concepts are important aspects of the journey towards becoming like the Elohim. He also presents his vision for a future world in which humanity has reached a higher level of consciousness and is living in peace and harmony with one another and with the universe.

Like Raël's first book, "Extraterrestrials Took Me to Their Planet" has been widely criticized for its claims of encounters with extraterrestrial beings and for its teachings about the nature of reality and the purpose of life. The Raëlian Movement, which is based on the teachings presented in the book, has also been the subject of controversy and has been accused of being a cult.

Despite these criticisms, the Raëlian Movement has continued to attract followers and has grown into a global organization with members in many countries. The book remains an important part of the Raëlian philosophy, and is considered by Raëlians to be a key source of knowledge and inspiration.

Overall, "Extraterrestrials Took Me to Their Planet" is a controversial and unconventional work that presents a highly imaginative view of the world and of human existence. Whether or not its claims and teachings are accepted by the wider public, the book remains an important expression of one person's vision of reality and of humanity's place in the universe.

![Image](images/le-message-book.jpg "Extraterrestrials Took Me To Their Planet, 1976 — Raël")

### About Claude Vorhilon

Claude Vorilhon, who goes by the name Raël, is a French spiritual leader and founder of the Raelian movement. Born in 1946, Raël worked as a race car driver, journalist, and singer before claiming to have a spiritual encounter with extraterrestrial beings in 1973.

According to Raël, these beings instructed him to spread their message of peace and love to humanity and to establish an embassy for them on Earth. He founded the Raelian movement in 1974, and it has since grown to become a global organization with thousands of members around the world.

The Raëlian movement's teachings draw on a mix of science, religion, and philosophy, and emphasize the idea that humans are descended from extraterrestrial beings

### Read it for yourself

📖  [Rael II — Extraterrestrials Took Me To Their Planet](https://wheelofheaven.github.io/rael-two-extraterrestrials-took-me-to-their-planet/)

### See also

[Resources › The Book Which Tells The Truth]({{< relref "resources/the-book-which-tells-the-truth/index.md" >}})</br>
[Wiki › Raëlism]({{< relref "wiki/encyclopedia/raelism.md" >}})</br>

### External links

[Download The Message | rael.org](https://www.rael.org/downloads/)
