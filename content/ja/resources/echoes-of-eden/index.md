---
title: "Echoes of Eden: What secrets of human potential were buried with our ancestors' memories of ET contact?"
description: "Paul Anthony Wallis — Echoes of Eden: What secrets of human potential were buried with our ancestors' memories of ET contact? (2022)"
lead: "Ancient stories from around the world describe entities which today we would call ETs. What other secrets lie hidden in the world's ancestral narratives? From Senate briefings in Washington DC to secret ceremonies in southern Africa, from strange phenomena in Australia and Iraq to mysterious encounters in modern Brazil and ancient Greece, Echoes of Eden will take you around the globe to discover why Military, Intelligence and other government agencies are so interested in archeology, indigenous rituals and traditional initiation practices. What is the connection between higher cognitive powers like remote viewing and precognition and ET contact in the deep past? And what are the implications for you and me?"
author: "Paul Anthony Wallis"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://www.amazon.com/dp/0645418307/"
menu:
  resources:
    parent: "browse"
weight: 320
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Ancient Astronauts", "Elohim", "Neo-Euhemerism"]
---

### About Paul Anthony Wallis

Paul Anthony Wallis is an author, researcher, and speaker, who has written several books on the topics of early Christianity, Gnosticism, and the origins of humanity. He is best known for his books "Scars of Eden" and "Echoes of Eden," which explore the possibility that the biblical story of Adam and Eve is based on ancient accounts of humanity's origins and interactions with extraterrestrial beings.

Wallis has a background in theology, history, and archaeology, and has spent over two decades researching the origins of the Judeo-Christian tradition. He has also studied Gnosticism, a set of religious and philosophical beliefs that emerged in the early Christian era and were considered heretical by the Church.

In addition to his writing, Wallis has given presentations on his research at conferences and events around the world. He has also appeared on various television programs and podcasts to discuss his work.
