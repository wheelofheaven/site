---
title: "Hamlet's Mill: An Essay Investigating the Origins of Human Knowledge and Its Transmission Through Myth"
description: "De Santillana & Von Dechend — Hamlet's Mill: An Essay Investigating the Origins of Human Knowledge and Its Transmission Through Myth (1969)"
lead: "Ever since the Greeks coined the language we commonly use for scientific description, mythology & science have developed separately. But what came before the Greeks? What if we could prove that all myths have one common origin in a celestial cosmology? What if the gods, the places they lived & what they did are but ciphers for celestial activity, a language for the perpetuation of complex astronomical data? Drawing on scientific data, historical & literary sources, the authors argue that our myths are the remains of a preliterate astronomy, an exacting science whose power & accuracy were suppressed & then forgotten by an emergent Greco-Roman world view. This fascinating book throws into doubt the self-congratulatory assumptions of Western science about the unfolding development & transmission of knowledge. This is a truly seminal & original thesis, a book that should be read by anyone interested in science, myth & the interactions between the two."
author: "Giorgio de Santillana & Hertha von Dechend"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://wheelofheaven.github.io/rael-two-extraterrestrials-took-me-to-their-planet/"
menu:
  resources:
    parent: "browse"
weight: 230
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Maintained Books"]
topics: ["Age of Aquarius", "Builder race", "Mythology", "Precession"]
---

"Hamlet's Mill: An Essay Investigating the Origins of Human Knowledge and Its Transmission Through Myth" is a book written by Giorgio de Santillana and Hertha von Dechend. The book explores the idea that much of human knowledge and culture, including science and mythology, has been transmitted through the ages in the form of symbolic stories and legends.

The authors argue that many myths from around the world contain hidden references to astronomical and scientific concepts, and that these myths were used as a way of preserving and transmitting scientific knowledge from one generation to the next. For example, they point to the many stories of a great flood that are found in cultures around the world, and argue that these tales may be based on astronomical observations of cometary or meteor impacts.

The authors also examine the use of mill imagery in myths and legends, such as the idea of the "Mill of the Gods," and argue that this imagery is a symbolic representation of the motions of the heavens, including the precession of the equinoxes and the cycles of the stars and planets.

"Hamlet's Mill" is written in an interdisciplinary style, drawing on evidence from fields such as mythology, astronomy, history, and archaeology, to support its central argument. The book has been influential in a number of different academic fields, including the study of mythology and comparative religion, and has been widely discussed and debated by scholars and researchers.

Overall, "Hamlet's Mill" is a fascinating and thought-provoking book that offers a unique perspective on the origins of human knowledge and its transmission through myth and symbolism. The authors' ideas have challenged traditional assumptions about the role of myth in human culture and the relationship between science and religion, and continue to be a subject of ongoing discussion and research.

![Image](images/hamlets-mill-book.jpg "Hamlet's Mill, 1969 — De Santillana & Von Dechend")

### Read it for yourself

📖  [Hamlet\'s Mill: An Essay Investigating the Origins of Human Knowledge and Its Transmission Through Myth](https://wheelofheaven.github.io/de-santillana-von-dechend-hamlets-mill/)

### See also

[Wiki › Great Year]({{< relref "wiki/encyclopedia/great-year.md" >}})</br>
[Wiki › Precession]({{< relref "wiki/encyclopedia/precession.md" >}})</br>
[Wiki › World Age]({{< relref "wiki/encyclopedia/world-age.md" >}})</br>

### External links

[Hamlet\'s Mill | Wikipedia](https://en.wikipedia.org/wiki/Hamlet%27s_Mill)
