---
title: "The Zohar: Pritzker Edition"
description: "The Zohar: Pritzker Edition by Daniel Matt (translation and commentary), 2018"
lead: "This is the first translation ever made from a critical Aramaic text of the Zohar, which has been established by Professor Daniel Matt based on a wide range of original manuscripts. The work spans twelve volumes. The extensive commentary, appearing at the bottom of each page, clarifies the kabbalistic symbolism and terminology, and cites sources and parallels from biblical, rabbinic, and kabbalistic texts."
author: "Daniel Matt (translation and commentary)"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://www.sup.org/zohar/"
menu:
  resources:
    parent: "browse"
weight: 320
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Archive", "Bible", "Kabbalah", "The Tradition"]
---

"The Zohar: Pritzker Edition" is a translation of the Zohar, a primary text of the Kabbalah, by Daniel Matt. The Zohar is a collection of mystical and esoteric teachings and commentary on the Torah, and it is considered one of the most important works of Jewish mysticism.

In "The Zohar: Pritzker Edition," Daniel Matt presents a new English translation of the text, along with extensive commentary and notes to help readers understand the teachings of the Zohar. The translation is based on the original Aramaic text of the Zohar, and it is designed to be both accessible and accurate.

Matt's translation of the Zohar provides a comprehensive overview of the Kabbalistic worldview and the teachings of the Zohar. It covers a wide range of topics, including the nature of God, the role of humanity, the structure of the universe, and the relationship between the physical and spiritual realms.

One of the key themes of the Zohar is the idea of the Ein Sof, which is the infinite and unknowable aspect of God. The text also discusses the sefirot, the ten attributes or emanations of God, which are seen as the building blocks of the universe. The Zohar teaches that the ultimate goal of humanity is to reunite with the Ein Sof and to attain spiritual enlightenment.

In "The Zohar: Pritzker Edition," Matt provides in-depth commentary and analysis of the text, drawing on his extensive knowledge of Kabbalah and Jewish mysticism. He also provides historical and cultural context for the teachings of the Zohar, and he explores the relationship between the Zohar and other mystical texts and traditions.

Overall, "The Zohar: Pritzker Edition" is a comprehensive and accessible translation of the Zohar that provides readers with a deeper understanding of this important and influential text. Whether you are a student of Kabbalah, a practitioner of Jewish mysticism, or simply someone interested in exploring the wisdom of the ancient mystics, this translation is a valuable resource that provides insights into the teachings of the Zohar and the Kabbalistic worldview.

### About Daniel Matt

Professor Daniel Matt is a scholar of Jewish mysticism and Kabbalah, and the translator of "The Zohar: Pritzker Edition," a multivolume, annotated translation of the classic work of Jewish mysticism known as the Zohar.

Matt holds a PhD from Brandeis University and has taught at several institutions, including the Graduate Theological Union in Berkeley, California, where he served as a professor of Jewish spirituality and mysticism. He has written several books and articles on Jewish mysticism, including "The Essential Kabbalah," "God and the Big Bang," and "Zohar: Annotated and Explained."

Matt's translation of the Zohar is widely regarded as one of the most comprehensive and accurate translations of the text to date. In addition to the text itself, the Pritzker Edition includes extensive commentary, notes, and essays that help to contextualize the work and make it more accessible to contemporary readers.

Matt has been recognized for his contributions to the study of Jewish mysticism and Kabbalah, and has received several awards and honors for his work. He continues to be a prominent figure in the world of Jewish scholarship and mysticism.

![Image](images/the-zohar-pritzker-edition-book.jpg "The Zohar, Pritzker Edition 2018 — Translation and commentary by Daniel Matt")
