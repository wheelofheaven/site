---
title: "La Révélation des Pyramides"
description: "The Revelation of the Pyramids: A French documentary by Jacques Grimault and Patrice Pooyard about the Great Pyramids from the year 2010."
lead: "The Revelation of the Pyramids: A French documentary by Jacques Grimault and Patrice Pooyard about the Great Pyramids from the year 2010."
author: "Jacques Grimault, Patrice Pooyard"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://fr.wikipedia.org/wiki/La_R%C3%A9v%C3%A9lation_des_Pyramides"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Documentaries"]
topics: ["Age of Aquarius", "Builder Race", "Neo-Euhemerism", "Precession", "Pyramids", "The Tradition"]
---

### Synopsis

The Revelation of the Pyramids (French: La Révélation des Pyrmadies) is a French documentary film produced by Jacques Grimault and Patrice Pooyard in 2010. The film presents the theory that the pyramids of Egypt were built by a highly advanced civilization in prehistoric times, and that the pyramids were not merely tombs for pharaohs, but served a much greater purpose. The filmmakers assert that the pyramids were constructed to reflect the movements of the stars and to preserve ancient knowledge, including advanced mathematical and astronomical understanding.

The documentary explores the astronomical alignments of the pyramids and the use of sacred geometry in their construction, including the Fibonacci sequence and the golden ratio. The film also examines the ancient legends and myths associated with the pyramids, and how these stories may be connected to the true purpose of the pyramids.

In addition, the filmmakers assert that the pyramids were designed to serve as a kind of energy generator, using the Earth's magnetic field to create a powerful source of energy. They also claim that the pyramids were used to store and transmit knowledge, and that the secrets they contain are still waiting to be uncovered.

Overall, The Revelation of the Pyramids presents a unique and controversial perspective on the purpose and significance of the pyramids of Egypt, challenging many of the traditional beliefs about these ancient structures and suggesting that they may have played a much more important role in human history than previously believed.

### See also

- [Wiki › Ancient Builders]({{< relref "wiki/encyclopedia/ancient-builders.md" >}})
- [Wiki › Sacred Geometry]({{< relref "wiki/encyclopedia/sacred-geometry.md" >}})

### External links

- [The Revelations of the Pyramids | Wikipedia](https://en.wikipedia.org/wiki/The_Revelation_of_the_Pyramids)
