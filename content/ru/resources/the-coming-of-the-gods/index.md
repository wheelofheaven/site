---
title: "The Coming Of The Gods"
description: "Jean Sendy — The Coming Of The Gods (1973)"
lead: ""
author: "Jean Sendy"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: ""
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Ancient Astronauts", "Elohim", "Neo-Euhemerism", "Precession"]
---

"The Coming of the Gods" is a book written by French author Jean Sendy, which explores the idea that advanced extraterrestrial civilizations have been visiting Earth for thousands of years and have played a major role in human history and religious beliefs.

Sendy argues that these extraterrestrial civilizations, which he refers to as "gods", have been responsible for many of the technological and cultural advancements in human history, as well as for the creation of religious beliefs and the inspiration for many ancient myths and legends. He suggests that the gods have interacted with humans in various ways over the centuries, including through direct communication, the creation of religious texts and symbols, and the performance of miracles.

The author draws on a wide range of historical and archaeological evidence, as well as recent developments in science and technology, to support his claims. He provides detailed accounts of ancient civilizations and their interactions with the gods, and shows how these interactions have shaped the course of human history and the development of religion.

"The Coming of the Gods" is written in a highly imaginative and imaginative style, and is intended to be both a work of scientific exploration and a philosophical meditation on the nature of religion and human spirituality. The book has been highly influential among certain spiritual and alternative communities, and is still widely read and studied by those interested in the relationship between extraterrestrial life and human beliefs.

Overall, "The Coming of the Gods" is a thought-provoking and innovative book that offers a unique perspective on human history and spirituality. Whether or not the author's ideas are ultimately accepted by the wider academic community, the book remains an important and influential work in the field of alternative history and extraterrestrial studies.

### About Jean Sendy

Jean Sendy was a French author and esoteric writer who lived from 1910 to 1978. He is best known for his work on ancient astronauts and extraterrestrial intervention in human history. In his books, such as "Those Gods Who Made Heaven and Earth" and "The Coming of the Gods," Sendy explored the idea that human civilization was influenced or even created by beings from other planets.

Sendy's work drew on a range of sources, including mythology, ancient texts, and scientific theories, to develop his ideas. He argued that many ancient cultures and religions had similar myths and legends about gods or divine beings who came from the sky, and that these stories could be interpreted as evidence of extraterrestrial contact.

While Sendy's ideas were not widely accepted by mainstream scholars, his work had a significant impact on popular culture, and influenced later writers and researchers in the fields of ufology, ancient astronaut theory, and the search for extraterrestrial life.

### See also

[Wiki › Neo-Euhemerism]({{< relref "wiki/encyclopedia/neo-euhemerism.md" >}})</br>
[Resources › L\'Ère du Verseau: Fin de l\'Illusion Humaniste by Jean Sendy \[FR\]]({{< relref "resources/l-ere-du-verseau/_index.md" >}})</br>
[Resources › Those Gods Who Made Heaven and Earth by Jean Sendy]({{< relref "resources/those-gods-who-made-heaven-and-earth/_index.md" >}})</br>

### External links

[The Coming Of The Gods | Google Books](https://books.google.ch/books?id=nlVlAAAACAAJ&sitesec=reviews&redir_esc=y)</br>
[The Coming Of The Gods | GoodReads](https://www.goodreads.com/book/show/4977839-the-coming-of-the-gods)</br>
