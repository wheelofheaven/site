---
title: "The Naked Bible"
description: "Mauro Biglino, Giorgio Cattaneo — The Naked Bible. 2022"
lead: "Writer and journalist Giorgio Cattaneo sits down with Italy's most renowned biblical translator for his first long interview about his life's work for the English audience. A decade long official Bible translator for the Church and lifelong researcher of ancient myths and tales, Mauro Bilglino is a unicum in his field of expertise and research. A fine connoisseur of dead languages, from ancient Greek to Hebrew and medieval Latin, he focused his attention and efforts on the accurate translating of the bible."
author: "Mauro Biglino, Giorgio Cattaneo"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://www.maurobiglino.com/libri/"
menu:
  resources:
    parent: "browse"
weight: 310
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Elohim", "Exegesis", "Neo-Euhemerism"]
---

"The Naked Bible" is a book written by the Italian journalist, playwright and screenwriter Giorgio Cattaneo and the Italian scholar Mauro Biglino, which challenges the traditional understanding of the Bible. In their book, Cattaneo writes about how Biglino argues that the texts of the Old Testament, specifically the Torah, were not divinely inspired, but rather were written by ancient scribes who recorded the history and beliefs of their society. Therein, Biglino claims that the text has been mistranslated and misinterpreted over time, and that a more accurate understanding of the Bible would reveal that it is not a divine text, but rather a historical document that reflects the cultural and political context of its authors.

### About Mauro Biglino

Translator of 19 books of the Old Testament for the prestigious Edizioni San Paolo, Mauro Biglino is an author, essayist and scholar of the history of religions who has been well known to the general public since 2010 with the publication of his first astonishing textual re-reading of the Bible, through 14 highly successful essays.

His work has earned him a great many enthusiasts, scholars and ordinary readers, whom he has guided – without prejudice or theological filters – through the fascinating narration of the biblical verses, examined in the original Hebrew. Biglino has worked with countless experts (biologists, archaeologists, engineers and doctors) who have supported his hypotheses regarding the possibility that the Bible contains evidence of the true origin of humanity, literally “manufactured” by a group of non-terrestrial individuals, the Elohim, subsequently transformed into divinities, starting with the first of them, Yahweh, who in the Old Testament is merely the “governor” of the Israelites, one of the many Jewish families.

A scholar of various ancient languages, Biglino has drawn suggestive parallels between the biblical narrative and contemporary traditions, from Egyptian to Vedic-Indian, passing through Greco-Roman culture, Homeric literature and the mysterious “biblical” traces that are so plentiful in the toponymy of the Baltic Sea.

In 2016, Biglino addressed his themes in a memorable meeting with both Christian and Jewish theologians. His position is clear. "I have never dealt with the transcendent God. I limit myself to observing that he is not present in the Bible."

His essays have been translated into English, Spanish, French, German, Portuguese, Dutch, Czech, Serbo-Croatian and Latvian. Over the past ten years, Mauro Biglino has worked tirelessly to inform and educate, disseminating his message in hundreds of conferences and participating in international conventions. Since 2020, all of his videos – viewed by millions of from all over the world – are collected on his YouTube channel: [Mauro Biglino Official Channel](https://www.youtube.com/@MauroBiglinoOfficialChannel).

### Giorgio Cattaneo

Giorgio Cattaneo has worked as a journalist, screenwriter, playwright and author of documentaries. He has collaborated with the famed journalist Giulietto Chiesa and has met people like Mikhail Gorbachev and the Pakistani leader Benazir Bhutto. He contributed to the draft of the film Siberian Education, by Oscar-winning Director Gabriele Salvatores and took part in the making of the documentary Qui, by
Daniele Gaglianone.

He published the novel A Valley at the bottom of the Wind (Aliberti) and the mini-stories The Lottery of the Universe (Youcanprint). For the publisher Graffio, he has published “Acts of Light”, a poetic portrait of the artist Tino Aime. He wrote the play Ak, the Chant of the Cathars, performed in 2006 with Eugenio Allegri and the participation of Cochi Ponzoni, the writer Maurizio Maggiani and the singer Antonella Ruggiero, for whom he (together with Gilberto Richiero) wrote the song “Niente di Noi” (Nothing of Us), for her album When I was a Singer.

"Encountering Mauro Biglino’s work," he says, "is a deeply healthy, stimulating and inevitably destabilizing. It forces us to reconsider the solidity of the awareness that nourishes many of our common beliefs. And it is a testament to the courage that is needed, today more than ever, to claim the full dignity of free research."
