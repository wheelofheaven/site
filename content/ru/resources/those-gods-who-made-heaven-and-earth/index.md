---
title: "Those Gods Who Made Heaven and Earth: The Evidence for Alien Visitors to Earth before the Dawn of History"
description: "Jean Sendy — Those Gods who made Heaven and Earth: The Evidence for Alien Visitors to Earth before the Dawn of History (1972)"
lead: ""
author: "Jean Sendy"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: ""
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Ancient Astronauts", "Elohim", "Neo-Euhemerism", "Precession"]
---

"Those Gods Who Made Heaven and Earth: The Evidence for Alien Visitors to Earth before the Dawn of History" is a book written by Jean Sendy, a French author and researcher. The book presents the author's views on the origins of ancient cultures and their possible connection to extraterrestrial visitors. According to Sendy, there is evidence that aliens have visited the Earth in the past, and that they have influenced the development of human civilizations, shaping the religious beliefs and practices of ancient peoples.

The book argues that many of the myths, legends, and religious texts of ancient cultures contain references to extraterrestrial visitors, and that these references can be understood as evidence of real encounters between humans and aliens. Sendy asserts that ancient cultures like the Sumerians, Egyptians, and Mayans had knowledge and technology that far exceeded their apparent level of development, and that this can only be explained by the influence of extraterrestrial visitors.

Sendy also examines various archaeological and architectural sites, such as the pyramids of Egypt and Mexico, as evidence of alien influence. He argues that these sites were constructed with technologies and knowledge beyond the abilities of the cultures that built them, and that they may have served as landing sites or communication centers for extraterrestrial visitors.

The book has been the subject of much debate and controversy, with some accepting the author's claims as credible, while others have criticized them as speculative and unsupported by mainstream science. Regardless, "Those Gods Who Made Heaven and Earth" remains a popular and widely discussed work in the field of ufology and alternative archaeology.

### About Jean Sendy

Jean Sendy was a French author and esoteric writer who lived from 1910 to 1978. He is best known for his work on ancient astronauts and extraterrestrial intervention in human history. In his books, such as "Those Gods Who Made Heaven and Earth" and "The Coming of the Gods," Sendy explored the idea that human civilization was influenced or even created by beings from other planets.

Sendy's work drew on a range of sources, including mythology, ancient texts, and scientific theories, to develop his ideas. He argued that many ancient cultures and religions had similar myths and legends about gods or divine beings who came from the sky, and that these stories could be interpreted as evidence of extraterrestrial contact.

While Sendy's ideas were not widely accepted by mainstream scholars, his work had a significant impact on popular culture, and influenced later writers and researchers in the fields of ufology, ancient astronaut theory, and the search for extraterrestrial life.

### See also

[Wiki › Neo-Euhemerism]({{< relref "wiki/encyclopedia/neo-euhemerism.md" >}})</br>
[Resources › L\'Ère du Verseau: Fin de l\'Illusion Humaniste by Jean Sendy \[FR\]]({{< relref "resources/l-ere-du-verseau/_index.md" >}})</br>
[Resources › The Coming Of The Gods by Jean Sendy]({{< relref "resources/the-coming-of-the-gods/_index.md" >}})</br>

### External links

[Those Gods Who Made Heaven and Earth | Google Books](https://books.google.ch/books?printsec=frontcover&vid=ISBN0425021300)</br>
[Those Gods Who Made Heaven and Earth | GoodReads](https://www.goodreads.com/en/book/show/2534402.Those_Gods_Who_Made_Heaven_Earth_The_Novel_Of_The_Bible)</br>
