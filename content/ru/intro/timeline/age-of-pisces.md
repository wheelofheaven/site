---
title: "Эпоха Рыб"
period: "210 г. до н.э.—1950 г."
description: "Wheel of Heaven - это база знаний, исследующая рабочую гипотезу о том, что жизнь на Земле была разумно спроектирована внеземной цивилизацией, так называемыми Элохим.."
excerpt: "Wheel of Heaven - это база знаний, исследующая рабочую гипотезу о том, что жизнь на Земле была разумно спроектирована внеземной цивилизацией, так называемыми Элохим.."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "timeline"
weight: 255
toc: true
---

The Age of Pisces lasted from –210 BC to 1950 CE and followed the Age of Aries which lasted up to –210. This World Age just ended very recently and the transition from the Age of Pisces to the new Age of Aquarius is still being felt.

![Image](images/equinox_bc210.png "Vernal equinox in 210 BC")

## The importance of the Biblical scriptures

> Christ’s role was to spread the truth of the biblical scriptures throughout the world, so that they could serve as proof for all of humanity when the age of science would finally explain everything.

The significance of Biblical scriptures lies in their ability to convey essential truths about our existence and the world around us. Christ's mission was to disseminate these truths globally, ensuring their preservation for future generations. In the age of science, the knowledge found in the Bible can be better understood, providing evidence and insights for humanity.

If we consider the Age of Aquarius, which began in 1945 with the development of nuclear power and the advent of information systems, to be synonymous with the age of science, several implications arise. The Age of Aquarius, which will be the main focus in the next chapter bearing the same name, is often associated with a time of increased knowledge, technology, and human potential. In this context, the age of science represents a period where humanity gains a deeper understanding of the world, enabling us to reexamine and reinterpret ancient wisdom, such as the Biblical scriptures, through a modern lens. During the Age of Aquarius, scientific advancements have led to remarkable discoveries and innovations, which have significantly shaped our understanding of the universe and our place within it. The development of nuclear power, for instance, has not only revolutionized energy production but also provided us with a glimpse into the immense power contained within the fabric of matter itself. Similarly, the arrival of information systems has transformed our ability to communicate, share knowledge, and access vast amounts of information at an unprecedented scale.

## Jesus Christ

> The creators therefore decided to arrange for a child to be born of a woman of the Earth and one of their own people. The child in question would thereby inherit certain telepathic faculties, which humans lack:
>
>> She was found with child of the Holy Ghost. Matthew 1: 18.
