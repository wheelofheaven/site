---
title: "智能设计"
description: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
excerpt: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 320
toc: true
---

关于地球生命起源的广泛接受的科学解释是自然起源理论，该理论认为生命通过一系列自然化学过程从简单有机化合物中产生。

地球生命由外星生物创造的想法肯定是对传统科学和宗教解释的偏离。

虽然这通常被认为是边缘信仰或甚至伪科学，但Raelism的智能设计概念提出了关于生命和智能构成的引人深思的问题，以及这些因素在物种发展中的作用。
