---
title: "知识库"
description: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
excerpt: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 410
toc: true
---

您已经通过了介绍部分，但是好奇心的旅程并没有在这里结束。务必查看Wheel of Heaven的维基、资源和博客部分，深入了解这里提出的大故事。

- [一个信息丰富的维基部分，充满条目和有趣的内容🔗]({{< ref "/wiki/" >}})
- [一个由我们团队精心挑选的网络书籍资源区🔗]({{< ref "/resources/" >}})
- [一个经常更新的博客，分享新鲜的观点和见解🔗]({{< ref "/articles/" >}})
