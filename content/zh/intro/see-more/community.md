---
title: "社区"
description: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
excerpt: "Wheel of Heaven是一个知识库，探索地球生命是否被所谓的外星文明Elohim有意识地设计的工作假设。"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 420
toc: true
---

如果Wheel of Heaven的前提让您更加好奇，您也可以参与其中，帮助我们添加缺失的信息或将不可用的内容翻译成其中一种支持的语言，使体验更加有价值。

您还可以通过Telegram、Github和/或Twitter与我们联系。在页眉中，您只需点击相应的图标，即可找到到达这些平台的链接。

与我们联系的最直接方式是通过电子邮件。请从我们的[联系方式]({{< ref "/contact" >}})页面获取当前官方电子邮件地址。
