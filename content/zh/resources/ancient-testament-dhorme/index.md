---
title: "Ancient Testament: Tome 1 et 2"
description: "Edouard Dhorme — La Bible: Ancient Testament (1956)"
lead: "La Bibliothèque de la Pléiade avait inscrit depuis longtemps à son programme une traduction intégrale de La Bible. Cette traduction devait être, par ses qualités littéraires, digne des grands classiques français et étrangers qui ont établi le renom de la collection. Elle devait en même temps répondre aux exigences de précision qu'ont suscitées le développement de l'esprit scientifique, les progrès de la philologie et les découvertes archéologiques les plus récentes. Nul ne pouvait donc être plus qualifié pour diriger et réaliser cette publication que M. Edouard Dhorme, membre de l'Institut, professeur honoraire au Collège de France : à une connaissance parfaite de l'hébreu et des langues sémitiques antérieures ou postérieures à celle-ci, M. Dhorme joint, à un haut degré, le sens de la langue française. Pour la première fois en France, semble-t-il, un tel approfondissement de l'hébreu non seulement n'a pas empâté la vigueur, ni terni les nuances de notre langue, mais au contraire en a affiné les richesses. C'est en serrant l'original de plus près que le traducteur, a, du fond du génie français, fait surgir des pouvoirs endormis et comme une nouvelle écriture. Celle-ci épouse le style de chacun des auteurs originaux et rend sensible leur tempérament propre : ici un ton oral sans âge, ailleurs de savants effets littéraires, parfois la raideur des inscriptions archaïques ou le frémissement de vie et la jeunesse retrouvée de poèmes immortels. L'introduction et les notes, n'ayant point de thèses à défendre, soucieuses uniquement d'éclairer le texte, situent tout ce qui peut l'être dans l'état actuel de nos connaissances : coutumes, jeux de mots, histoire et géographie, philosophie et morale, etc. Elles portent la marque d'une grande sagesse et d'une prudence courageuse. M. Dhorme, qui connaît aussi bien les hardiesses hypercritiques que la théologie savante, sait défendre les droits du texte littéral contre toute interprétation tendancieuse et se réserver devant les hypothèses téméraires. Voilà qui ne saurait laisser indifférents ni les croyants ni les historiens : cette publication doit ainsi emporter l'assentiment unanime. Il se trouve de surcroît que c'est un grand événement littéraire"
author: "Edouard Dorme"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://www.la-pleiade.fr/Catalogue/GALLIMARD/Bibliotheque-de-la-Pleiade/La-Bible4"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Bible", "Elohim"]
---

![Image](images/dhorme-bible-books.jpg "La Bible: Ancient Testament, Tome I & Tome II (1956) — Edouard Dhorme")

### About Edouard Dhorme

Edouard Dhorme (1881-1966) was a French Assyriologist and biblical scholar. He is best known for his work on the Akkadian language and the cuneiform script, as well as his translations and commentaries on the Hebrew Bible and the Dead Sea Scrolls.

Dhorme studied at the Ecole biblique et archéologique française de Jérusalem, where he developed his expertise in Semitic languages and literature. He later taught at several universities, including the Sorbonne in Paris, where he was appointed professor of Assyriology in 1933.

Dhorme's most influential works include "The Ancient Near East," a survey of the history, culture, and religions of the region, and "Theologie babylonienne et judéo-chrétienne," a comparative study of Babylonian and Judeo-Christian theology. He also contributed to the French translation of the Bible known as the "Bible de Jérusalem," and published several commentaries on individual books of the Hebrew Bible, including the Psalms and the Book of Job.

Dhorme's contributions to the fields of Assyriology and biblical scholarship are still recognized and studied today.

### See also

[Wiki › Dhorme Bible translation]({{< relref "wiki/encyclopedia/dhorme-bible-translation.md" >}})</br>

### External links

[La Bible: Ancient Testament | Bibliothèque de la Pléiade](https://www.la-pleiade.fr/Catalogue/GALLIMARD/Bibliotheque-de-la-Pleiade/La-Bible4)
