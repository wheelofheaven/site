---
title: "A Gentleman’s Joyous Esotericism: Jean Sendy Above and Beyond the Ancient Aliens"
description: "Abstract: the article reconstructs the narratives advanced by the author Jean Sendy (1910-1978). His life is reconstructed as well. It is argued that Sendy was a cultivated, sophisticated and ironic author, deeply different, by virtue of his books’ quality, from other proponents of the “ancient aliens” narratives with whom he is often paired."
lead: 
author: "Stefano Bigliardi"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-website.jpg"]
link: "https://www.academia.edu/24363391/_A_Gentleman_s_Joyous_Esotericism_Jean_Sendy_Above_and_Beyond_the_Ancient_Aliens_Alternative_Spirituality_and_Religion_Review_8_1_2017_1_35"
menu:
  resources:
    parent: "browse"
weight: 310
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Papers"]
topics: ["Age of Aquarius", "Ancient Astronauts", "Elohim", "Neo-Euhemerism", "Precession"]
---

In his paper "A Gentleman’s Joyous Esotericism: Jean Sendy Above and Beyond the Ancient Aliens," Stefano Bigliardi discusses the work of Jean Sendy, a French esoteric writer who explored the idea of ancient astronauts and extraterrestrial intervention in human history. Bigliardi provides an overview of Sendy's life and work, including his theories on the origins of humanity, the influence of extraterrestrial beings, and the possible connections between various mythologies and ancient cultures.

Bigliardi argues that while Sendy's ideas are often associated with the contemporary "ancient aliens" movement, his work is more nuanced and sophisticated than the simplistic and often sensationalist claims of some modern authors. Sendy drew on a wide range of sources, including mythology, ancient texts, and scientific theories, to develop his ideas. He also emphasized the need for critical thinking and careful analysis of evidence, rather than simply accepting or rejecting theories based on preconceived notions.

Overall, Bigliardi suggests that Sendy's work is an interesting and valuable contribution to the study of esotericism and the search for meaning in human history, and encourages readers to engage with his ideas in a thoughtful and critical manner.

### Abstract

The abstract that is taken straight from the paper itself:

> The article reconstructs the narratives advanced by the author Jean Sendy (1910-1978). His life is reconstructed as well. It is argued that Sendy was a cultivated, sophisticated and ironic author, deeply different, by virtue of his books’ quality, from other proponents of the “ancient aliens” narratives with whom he is often paired.

### About Stefano Bigliardi

Stefano Bigliardi is an Italian scholar of religion and philosopher. He is currently an associate professor of philosophy of science and religion at the Department of Educational Sciences of the University of Bergamo in Italy. He has published extensively on topics such as the relationship between science and religion, the history and philosophy of Islamic science, and the study of esotericism and mysticism.

### See also

[Wiki › Great Year]({{< relref "wiki/encyclopedia/great-year.md" >}})</br>
[Wiki › Precession]({{< relref "wiki/encyclopedia/precession.md" >}})</br>
