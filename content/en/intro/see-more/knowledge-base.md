---
title: "Knowledge base"
description: "Wheel of Heaven is a knowledge base exploring the working hypothesis that life on Earth was intelligently designed by an extraterrestrial civilization, the so-called Elohim."
excerpt: "Wheel of Heaven is a knowledge base exploring the working hypothesis that life on Earth was intelligently designed by an extraterrestrial civilization, the so-called Elohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 410
toc: true
---

You made it through the Introduction, but the inquisitive journey doesn't end here. Make sure to check out the Wiki, the Resources and the Blog section of Wheel of Heaven to dive more into the big narrative put forward here.

- [An informative wiki section filled with entries and points of interest 🔗]({{< ref "/wiki/" >}})
- [A resource area that features web books handpicked by our team 🔗]({{< ref "/resources/" >}})
- [A frequently updated blog where fresh perspectives and insights are shared 🔗]({{< ref "/articles/" >}})
