---
title: "Escaping from Eden: Does Genesis teach that the human race was created by God or engineered by ETs?"
description: "Paul Anthony Wallis — Escaping from Eden: Does Genesis teach that the human race was created by God or engineered by ETs? (2020)"
lead: "The familiar stories of the book of Genesis affirm that God made the universe, planet earth, and you and me. However, various anomalies in the text clue us that we are not reading the original version of these stories. So what were the original narratives and what did they say about who we are and where we all came from? What was the earlier story of human origins, almost obliterated from the Hebrew Scriptures in the 6th century BC, and suppressed from Christian writing in the 2nd and 3rd centuries AD? And what does any of this have to do with Extra Terrestrials? Escaping from Eden will take you on a journey around the world and into the mythologies of ancient Sumeria, Mesoamerica, India, Africa, and Greece to reveal a profound secret, hidden in plain sight in the text of the Bible. Far reaching and deeply controversial, this book points to truths about ourselves, the universe and everything that you may have long suspected but not dared to speak!"
author: "Paul Anthony Wallis"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://paulanthonywallis.com/2016/10/12/blog-post-title-2/"
menu:
  resources:
    parent: "browse"
weight: 310
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Ancient Astronauts", "Elohim", "Neo-Euhemerism"]
---

"Escaping from Eden: Does Genesis teach that the human race was created by God or engineered by ETs?" is a book written by Paul Anthony Wallis, an author and researcher in the fields of religion and extraterrestrial life. The book presents the author's views on the origins of humanity and the possible connection between human creation and extraterrestrial life.

The book argues that the biblical story of Adam and Eve in the book of Genesis may have been inspired by ancient knowledge of extraterrestrial visitations. Wallis asserts that the biblical story contains elements that suggest that the first humans were genetically engineered by extraterrestrial beings, rather than being created by a deity.

The author uses various biblical texts, along with ancient myths and legends, to support his argument that extraterrestrial beings played a role in human creation. He also examines evidence from archaeology and anthropology to suggest that ancient cultures had advanced knowledge of astronomy and genetic engineering, which he believes may have been passed down from extraterrestrial visitors.

The book has been met with criticism from some religious scholars and mainstream scientists, who reject the author's claims as unsupported and lacking credible evidence. Nevertheless, "Escaping from Eden" remains a popular and widely discussed work in the field of alternative religion and extraterrestrial life.

 ![Image](images/escaping-from-eden-book.jpg "Escaping from Eden, 2020 — Paul Anthony Wallis")
