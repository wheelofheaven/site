---
title: "Mazzaroth; or, The constellations"
description: "Frances Rolleston — Mazzaroth; or, The constellations (1862)"
lead: "
That work of her whole life was Mazzaroth: The Constellations, Parts I-IV, Including Mizraim: Astronomy of Egypt. Her theory and discoveries that the signs and constellations were designed to represent Messiah according to the prophecy in Genesis 3:15, that the Seed of the woman would crush the serpent. The outline was constructed in her youth, but a 20 to 30 year hiatus preceded the writing, which was revised, corrected, and arranged late in life. Mazzaroth was published in bits and parts during her lifetime, and as a whole a year after her death. But was it finished? Would she ever have finished? In the last letter she ever wrote, she enthused over new discoveries from her Egyptian planisphere, and stated that there was much yet to be found. Thus Frances fulfilled the wish she had written in her common-place book at about age 24: “Heaven defend me from an old age of novels."
author: "Frances Rolleston"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://www.francesrolleston.com/mazzaroth.html"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Bible", "Gospel in the Stars", "Mazzaroth", "Precession"]
---

"Mazzaroth; or, The constellations" is a book written by English author Frances Rolleston, which explores the connection between the stars and human history, religion, and spirituality.

The author argues that the constellations of the night sky are a record of ancient astronomical knowledge and wisdom, and that they contain hidden meanings and symbols that reveal the truth about the universe and our place in it. Rolleston draws on a range of biblical, mythological, and historical sources to support her claims, and provides detailed descriptions and interpretations of each of the major constellations.

At the heart of the book is the idea that the constellations represent the story of the world, from its creation to its eventual destruction, and that they offer insights into the nature of God, the human soul, and the purpose of life. Rolleston suggests that the constellations were originally used as a means of communicating spiritual truths and of guiding humanity towards a deeper understanding of the divine.

"Mazzaroth; or, The constellations" was written in the 19th century, and reflects the interest in spiritualism and esoteric knowledge that was prevalent at the time. The book is written in a poetic and imaginative style, and is intended to inspire the reader to look at the stars in a new light and to see the wonders of the universe from a spiritual perspective.

Overall, "Mazzaroth; or, The constellations" is a unique and fascinating work that offers a glimpse into the worldview of an earlier era and provides a bridge between the worlds of science, religion, and spirituality. Whether or not the author's ideas are accepted by the wider scientific community, the book remains an important and valuable contribution to the field of astronomy and the history of ideas.

![Image](images/mazzaroth-rolleston-book.jpg "Mazzaroth; or, The Constellations — Frances Rolleston")

### About Frances Rolleston

Frances Rolleston (1781-1864) was an English scholar, author, and theologian who is best known for her work on biblical symbolism and typology. She was born into a wealthy family and spent much of her life studying and writing about the Bible and Christian theology.

Rolleston's most significant work is "Mazzaroth; or, The Constellations," a study of the astrological symbolism of the Bible and the connection between the zodiac and biblical events. The book was published in 1865, a year after her death, and was based on her extensive research and notes.

In addition to her work on biblical symbolism, Rolleston was also involved in various philanthropic and social causes, including the education and welfare of women and children. She supported various charitable organizations and was a prominent advocate for women's rights and social justice.

Despite her contributions to the study of biblical symbolism and theology, Rolleston's work was largely overlooked by contemporary scholars, and her legacy was not fully recognized until many years after her death. Today, she is regarded as an important figure in the history of biblical studies and a pioneer of biblical typology and symbolism.

### See also

[Wiki › Precession]({{< relref "wiki/encyclopedia/precession.md" >}})</br>

### External links

[Mazzaroth; or, The constellations | Google Books](https://books.google.ch/books/about/Mazzaroth_or_The_constellations_by_F_Rol.html?id=hTABAAAAQAAJ)</br>
