---
title: "Wonders in the Sky: Unexplained Aerial Objects from Antiquity to Modern Times"
description: "Jacques Vallée & Chris Aubeck: Wonders in the Sky: Unexplained Aerial Objects from Antiquity to Modern Times, 2010"
lead: "In Wonders in the Sky, respected researchers Jacques Vallee and Chris Aubeck examine more than 500 selected reports of sightings from biblical-age antiquity through the year 1879-the point at which the Industrial Revolution deeply changed the nature of human society, and the skies began to open to airplanes, dirigibles, rockets, and other opportunities for misinterpretation represented by military prototypes. Using vivid and engaging case studies, and more than seventy-five illustrations, they reveal that unidentified flying objects have had a major impact not only on popular culture but on our history, on our religion, and on the models of the world humanity has formed from deepest antiquity."
author: "Jacques Vallée & Chris Aubeck"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://www.amazon.com/Wonders-Sky-Unexplained-Objects-Antiquity/dp/1585428205"
menu:
  resources:
    parent: "browse"
weight: 320
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Ancient Astronauts", "Archive", "Mythology", "Religion", "The Tradition"]
---

"Wonders in the Sky: Unexplained Aerial Objects from Antiquity to Modern Times" is a book by Jacques Vallée and Chris Aubeck that explores the history of sightings and reports of unexplained aerial objects throughout history. The authors examine accounts of strange objects in the sky from ancient civilizations, medieval times, and up to the present day, with a focus on patterns and themes that emerge in the data.

The book covers a wide range of incidents and experiences, including sightings of unidentified flying objects (UFOs), encounters with strange beings, and other anomalous phenomena. The authors draw on a variety of sources, including historical texts, eyewitness accounts, and scientific data, to build a comprehensive picture of the phenomenon and its evolution over time.

One of the key themes of the book is the idea that the phenomenon of unexplained aerial objects has been a consistent and persistent part of human experience, and that it is not simply a modern phenomenon. The authors argue that many of the reports of strange objects and experiences throughout history are consistent with modern UFO sightings, and that this suggests that the phenomenon is not a recent development, but rather a long-standing and recurring part of human experience.

Another important theme of the book is the idea that the phenomenon of unexplained aerial objects is not easily explained by conventional scientific or technological explanations. The authors argue that while some UFO sightings can be explained by natural phenomena or misidentifications, there is a significant body of evidence that suggests the existence of something more mysterious and unexplained.

The authors also explore the social and cultural aspects of the phenomenon, and the way in which people have responded to sightings and reports of strange objects in the sky throughout history. They examine the ways in which people have attempted to explain the phenomenon, both scientifically and mythologically, and the way in which it has been incorporated into different cultural and religious beliefs and practices.

Overall, "Wonders in the Sky: Unexplained Aerial Objects from Antiquity to Modern Times" provides a comprehensive overview of the history and evolution of the phenomenon of unexplained aerial objects, and offers a unique and thought-provoking perspective on one of the most intriguing and mysterious aspects of human experience. Whether you are a student of history, a researcher in the field of UFO studies, or simply someone with a curiosity about the unexplained, this book is a valuable and informative resource.

![Image](images/wonders-in-the-sky-book.jpg "Wonders in the Sky, 2010 — Jacques Vallée & Chris Aubeck")

### About Jacques Vallée

Jacques Vallee is a French-American computer scientist, author, and ufologist. Born in France in 1939, Vallee obtained a PhD in computer science from Northwestern University in the United States in 1967, and later worked at the Stanford Research Institute (SRI) in California.

Vallee is known for his research on unidentified flying objects (UFOs) and the paranormal. His best-known book, "Passport to Magonia: From Folklore to Flying Saucers," explores the links between traditional myths and legends and modern UFO sightings, and proposes that both may be rooted in a common phenomenon of human consciousness.

Vallee has also written extensively on the social and cultural implications of emerging technologies, and has been a prominent advocate for responsible innovation and ethical consideration of new technologies. He has been a consultant for several technology companies and organizations, and has served on the board of several scientific and academic institutions.

Vallee's work has been influential in the fields of ufology, consciousness studies, and the sociology of science. He has been praised for his rigorous research and thoughtful approach to controversial topics, and continues to be a prominent figure in the world of scientific inquiry and exploration.

### About Chris Aubeck

Chris Aubeck is a British researcher and writer who specializes in the study of historical and contemporary UFO sightings and encounters. He is known for his meticulous research and analysis of primary sources, including eyewitness accounts, newspaper articles, and government documents, to uncover new insights and perspectives on UFO phenomena.

Aubeck's work has been featured in numerous publications and media outlets, and he has authored several books on the subject of UFOs, including "Return to Magonia: Investigating UFOs in History" and "Secrets of the Mysterious Valley," which explore the history and cultural significance of UFO sightings around the world.

In addition to his work on UFOs, Aubeck has also written about a range of other topics related to paranormal phenomena and folklore, and has contributed to numerous academic and popular publications. He has also spoken at conferences and events around the world, and has been recognized for his contributions to the field of ufology and related fields.
