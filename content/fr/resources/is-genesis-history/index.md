---
title: "Is Genesis History?"
description: "Is Genesis History? Features over a dozen scientists and scholars explaining how the world intersects with the history recorded in Genesis. From rock layers to fossils, from lions to stars, from the Bible to artifacts, this fascinating film will change the way you see the world."
lead: "Features over a dozen scientists and scholars explaining how the world intersects with the history recorded in Genesis. From rock layers to fossils, from lions to stars, from the Bible to artifacts, this fascinating film will change the way you see the world."
author: "Dr. Del Tackett and Thomas Purifoy Jr."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://isgenesishistory.com/"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Documentaries"]
topics: ["Bible", "Genesis", "Religion"]
---

The following description can be found on the website of the "Is Genesis History?" website:

> Is Genesis History? features over a dozen scientists and scholars explaining how the world intersects with the history recorded in Genesis.  From rock layers to fossils, from lions to stars, from the Bible to artifacts, this fascinating film will change the way you see the world.
>
> The film’s goal is to provide a reasonable case for Creation in six normal days, a real Adam and Eve, an actual fall, a global flood, and a tower of Babel. Dr. Del Tackett, creator of The Truth Project, serves as your guide—hiking through canyons, climbing up mountains, and diving below the sea—in an exploration of two competing views … one compelling truth.

### Synopsis

"Is Genesis History?" is a 2017 documentary film by Del Tackett that presents a young Earth creationist perspective on the Biblical account of creation as described in the book of Genesis. The film features interviews with a number of scientists, researchers, and theologians who support a literal interpretation of the creation story.

The film argues that the scientific evidence supports the idea that the Earth is young, only thousands of years old, and that all life on Earth was created in six literal 24-hour days, as described in Genesis. It presents arguments against evolution and an old Earth, such as the apparent lack of intermediate fossil forms, the apparent sudden appearance of complex life forms in the fossil record, and the argument that the laws of thermodynamics make it impossible for the universe and life on Earth to have existed for billions of years.

Additionally, the film discusses various aspects of the creation story, including the creation of the universe, the creation of life, and the Biblical account of the Flood. It argues that the Flood was a global event that shaped the world as we know it today, and that the evidence of the Flood can be seen in the geological features of the Earth and in the fossil record.

The main arguments made in the film are:

- The Earth is young: The film argues that the scientific evidence supports the idea that the Earth is only thousands of years old, not billions as accepted by mainstream science.

- The Bible is literally true: The film presents the argument that the creation story in Genesis should be taken literally and that all life on Earth was created in six literal 24-hour days.

- Evidence against evolution: The film presents arguments against evolution, such as the apparent lack of intermediate fossil forms and the apparent sudden appearance of complex life forms in the fossil record.

- Evidence for the Flood: The film argues that the global Flood described in the Bible was a real event and that the evidence of the Flood can be seen in the geological features of the Earth and in the fossil record.

- Scientific validity of creationism: The film argues that creationism is a scientifically valid perspective and that it should be considered as an alternative to evolution and an old Earth.

It is important to note that the scientific claims made in the film have been challenged by the vast majority of the scientific community, and are not supported by mainstream science. The film's arguments and conclusions are considered to be controversial and not representative of the prevailing scientific consensus on the origin and age of the Earth and life on it.

Overall, "Is Genesis History?" presents a specific, young Earth creationist perspective on the creation story, and is intended to encourage viewers to consider this perspective as an alternative to evolution and an old Earth.

### See also

- [Graham Hancock\'s Fingerprints of the Gods\: The Evidence of Earth\'s Lost Civilization]({{< relref "resources/fingerprints-of-the-gods/index.md" >}})

### External links

- [Ancient Apocalypse | Wikipedia](https://en.wikipedia.org/wiki/Ancient_Apocalypse)
