---
title: "The Gospel in the Stars"
description: "Joseph A. Seiss — The Gospel in the Stars (1884)"
lead: "Do the heavens that reveal God's glory also reveal His plan of salvation? Yes, says Joseph A. Seiss, who contemplated the strange figures of the Zodiac and studied the writings of astronomers through the ages. Drawing upon scientific, historical and biblical sources, Seiss assembles persuasive arguments supporting his thesis that the Gospel of Jesus Christ can be seen in the stars."
author: "Joseph A. Seiss"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: ""
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Bible", "Gospel in the Stars", "Mazzaroth", "Precession"]
---

"The Gospel in the Stars" is a book written by Joseph Seiss, an American pastor and author, that explores the idea that the stars in the sky contain a hidden message about the gospel of Jesus Christ.

Seiss argues that the constellations and their arrangements in the sky have been used throughout human history as a means of transmitting knowledge and wisdom, and that they contain a message about the nature of God and the plan of salvation. He suggests that the constellations were designed by God as a way of communicating the gospel message to humanity, and that they provide a visual representation of the gospel story, including the birth, life, death, and resurrection of Jesus Christ.

The author explores each of the constellations in detail, offering a biblical interpretation of their stories and symbols, and showing how they fit into the larger picture of the gospel story. He also provides a historical overview of the use of the stars and the constellations in human culture, including their role in ancient astronomical systems, their use in navigation and timekeeping, and their significance in religious and spiritual traditions.

"The Gospel in the Stars" is written in a highly imaginative and imaginative style, and is intended to be both a biblical study and a devotional meditation on the gospel message. The book has been highly influential among certain Christian communities, and is still widely read and studied by those interested in the relationship between astronomy, the Bible, and Christian theology.

Overall, "The Gospel in the Stars" is a fascinating and inspiring book that offers a unique perspective on the gospel message and its relationship to the stars and the universe. Whether or not the author's ideas are ultimately accepted by the wider academic community, the book remains an important and influential work in the field of Christian theology and biblical interpretation.

![Image](images/gospel-in-the-stars-book.jpg "The Gospel in the Stars — Joseph A. Seiss")

### About Joseph A. Seiss

Joseph Augustus Seiss (1823-1904) was an American Lutheran minister and theologian, known for his work in biblical scholarship and his influential writings on the book of Revelation.

Seiss was born in Maryland and studied at the Lutheran Theological Seminary in Gettysburg, Pennsylvania. After graduating, he served as a pastor at several Lutheran churches in Pennsylvania and Maryland before becoming a professor of theology at the Philadelphia Divinity School.

Seiss was a prolific author and wrote extensively on biblical prophecy and eschatology. His most famous work is "The Apocalypse: Lectures on the Book of Revelation," a commentary on the final book of the New Testament that remains a popular and influential work to this day. In addition to his writing, Seiss was also an active speaker and lecturer, and often traveled throughout the United States to preach and give talks on biblical topics.

Seiss's work has been praised for its insight and scholarship, and his contributions to the study of biblical prophecy have had a lasting impact on Christian theology and eschatology. He is remembered as a significant figure in the history of American Christianity and a pioneer of biblical scholarship in the United States.

### See also

[Wiki › Precession]({{< relref "wiki/encyclopedia/precession.md" >}})</br>

### External links

[The Gospel in the Stars | Google Books](https://books.google.ch/books/about/The_Gospel_in_the_Stars.html?id=1DZMpDWbqR0C)</br>
