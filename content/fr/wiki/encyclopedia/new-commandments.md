---
title: "New Commandments"
description: "The New Commandments, a series of doctrines put forward by Yahweh that elaborate what humanity could take into account in order to progress and usher into the Golden Age, now at the beginning of the Age of Aquarius."
lead: "A series of doctrines put forward by Yahweh that elaborate what humanity could take into account in order to progress and usher into the Golden Age, now at the beginning of the Age of Aquarius."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  wiki:
    parent: "Wiki"
weight: 200
toc: true
---

## In Raëlism 🔯

The New Commandments are constituted of the following aspects:

- Geniocracy
- Humanitarnism
- World Government
- The mission of the propagation of the Truth

## See also

- [Wiki › Geniocracy]({{< relref "wiki/encyclopedia/geniocracy.md" >}})
- [Wiki › Humanitarianism]({{< relref "wiki/encyclopedia/humanitarianism.md" >}})

## Read more

- [Intro › Timeline › Age of Aquarius]({{< relref "intro/timeline/age-of-aquarius.md" >}})

## Read more

- [Resources › The Book Which Tells The Truth]({{< relref "resources/the-book-which-tells-the-truth/index.md" >}})
