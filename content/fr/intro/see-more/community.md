---
title: "Communauté"
description: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
excerpt: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 420
toc: true
---

Si les hypothèses de Wheel of Heaven ont suscité davantage de curiosité en vous, vous pouvez également participer à rendre l'expérience plus enrichissante en nous aidant à ajouter des informations manquantes ou à traduire le contenu indisponible dans l'une des langues prises en charge.

Vous pouvez également nous contacter via Telegram, Github et/ou Twitter. Dans l'en-tête, vous trouverez les liens vers chacune de ces plateformes en cliquant simplement sur l'icône correspondante.

Le moyen le plus simple de nous contacter est par e-mail. Obtenez l'adresse e-mail officielle actuelle sur notre page [Contact]({{< ref "/contact" >}}).
