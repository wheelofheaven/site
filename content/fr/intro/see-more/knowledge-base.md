---
title: "Base de connaissances"
description: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
excerpt: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 410
toc: true
---

Vous avez franchi l'introduction, mais le voyage curieux ne s'arrête pas là. Assurez-vous de consulter la section Wiki, les Ressources et le Blog de Wheel of Heaven pour plonger davantage dans la grande narration présentée ici.

- [Une section informative du wiki remplie d'articles et de points d'intérêt 🔗]({{< ref "/wiki/" >}})
- [Une zone de ressources présentant des livres en ligne sélectionnés par notre équipe 🔗]({{< ref "/resources/" >}})
- [Un blog régulièrement mis à jour où des perspectives fraîches et des idées sont partagées 🔗]({{< ref "/articles/" >}})
