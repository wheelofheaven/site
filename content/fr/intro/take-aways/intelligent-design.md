---
title: "Design intelligent"
description: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
excerpt: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 320
toc: true
---

L'explication scientifique généralement acceptée pour l'origine de la vie sur Terre est la théorie de l'abiogenèse, qui suggère que la vie est apparue à partir de composés organiques simples grâce à une série de processus chimiques naturels.

L'idée que des êtres extraterrestres ont créé la vie sur Terre est certainement une déviation des explications scientifiques et religieuses traditionnelles.

Bien qu'elle serait généralement considérée comme une croyance marginale ou même une pseudoscience, le concept de design intelligent du Raëlisme soulève des questions provocatrices sur ce qui constitue la vie et l'intelligence, et le rôle que ces facteurs jouent dans le développement des espèces.
