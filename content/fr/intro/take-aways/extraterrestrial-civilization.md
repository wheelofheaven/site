---
title: "Civilisation extraterrestre"
description: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
excerpt: "Wheel of Heaven est une base de connaissances explorant l'hypothèse de travail selon laquelle la vie sur Terre a été intelligemment conçue par une civilisation extraterrestre, les soi-disant Élohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 310
toc: true
---

Placer la vie terrestre dans un contexte cosmique plus large inspire un plus grand sens de l'interconnexion et suscite la curiosité quant à la possibilité d'autres formes de vie dans l'univers. Cela soulève également des questions sur le rôle potentiel des humains dans le cosmos et nos responsabilités en tant qu'espèce, nous invitant à réévaluer notre place dans le grand schéma des choses.

L'idée que la vie sur Terre a été créée par des êtres extraterrestres défie les perspectives anthropocentriques, qui placent les humains au centre de l'univers. En considérant la possibilité d'une origine extraterrestre pour la vie sur Terre, nous sommes invités à réfléchir à la signification et au but de la vie terrestre dans un contexte cosmique plus large. Cette perspective peut encourager une vision plus humble de notre place dans l'univers.
