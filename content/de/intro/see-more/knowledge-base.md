---
title: "Knowledge Base"
description: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
excerpt: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 410
toc: true
---

Du hast es durch die Einführung geschafft, aber die neugierige Reise endet hier nicht. Vergewissere dich, dass du das Wiki, die Ressourcen und den Blog-Bereich von Wheel of Heaven überprüfst, um tiefer in die hier vorgestellte große Erzählung einzutauchen.

- [Ein informativer Wiki-Bereich mit Einträgen und interessanten Punkten 🔗]({{< ref "/wiki/" >}})
- [Ein Ressourcenbereich mit von unserem Team ausgewählten Web-Büchern 🔗]({{< ref "/resources/" >}})
- [Ein häufig aktualisierter Blog, in dem frische Perspektiven und Erkenntnisse geteilt werden 🔗]({{< ref "/articles/" >}})
