---
title: "Community"
description: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
excerpt: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 420
toc: true
---

Wenn dich die Grundlagen von Wheel of Heaven neugierig gemacht haben, kannst du auch dazu beitragen, die Erfahrung noch wertvoller zu machen, indem du uns hilfst, fehlende Informationen hinzuzufügen oder nicht verfügbare Inhalte in eine der unterstützten Sprachen zu übersetzen.

Du kannst uns auch über Telegram, Github und/oder Twitter kontaktieren. Im Header findest du die Links zu jeder dieser Plattformen, indem du einfach auf das entsprechende Symbol klickst.

Der direkteste Weg, um uns zu kontaktieren, ist per E-Mail. Hol dir die aktuelle offizielle E-Mail-Adresse von unserer [Kontakt]({{< ref "/contact" >}})-Seite.
