---
title: "Intelligent Design"
description: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
excerpt: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 320
toc: true
---

Die allgemein anerkannte wissenschaftliche Erklärung für den Ursprung des Lebens auf der Erde ist die Theorie der Abiogenese, die vorschlägt, dass das Leben aus einfachen organischen Verbindungen durch eine Reihe natürlicher chemischer Prozesse entstanden ist.

Die Idee, dass außerirdische Wesen das Leben auf der Erde erschaffen haben, stellt sicherlich eine Abkehr von traditionellen wissenschaftlichen und auch religiösen Erklärungen dar.

Obwohl es allgemein als Randglaube oder sogar Pseudowissenschaft betrachtet werden würde, wirft das Konzept des Intelligenten Designs im Raëlismus thought-provoking Fragen auf, was Leben und Intelligenz ausmacht und welche Rolle diese Faktoren bei der Entwicklung von Arten spielen.
