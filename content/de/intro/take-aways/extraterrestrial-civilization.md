---
title: "Außerirdische Zivilisation"
description: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
excerpt: "Wheel of Heaven ist eine Wissensbasis, die die Arbeitshypothese untersucht, dass das Leben auf der Erde von einer außerirdischen Zivilisation, den sogenannten Elohim, intelligent entworfen wurde."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 310
toc: true
---

Das Leben auf der Erde in einen größeren kosmischen Zusammenhang zu stellen, inspiriert zu einem größeren Gefühl der Verbundenheit und weckt die Neugier auf die Möglichkeit anderer Lebensformen im Universum. Es wirft auch Fragen auf über die potenzielle Rolle der Menschen im Kosmos und unsere Verantwortung als Spezies und lädt uns ein, unseren Platz im großen Ganzen neu zu bewerten.

Die Vorstellung, dass das Leben auf der Erde von außerirdischen Wesen geschaffen wurde, stellt anthropozentrische Perspektiven in Frage, die den Menschen in den Mittelpunkt des Universums stellen. Durch die Überlegung der Möglichkeit einer außerirdischen Herkunft des Lebens auf der Erde werden wir eingeladen, über die Bedeutung und den Zweck des irdischen Lebens in einem größeren kosmischen Zusammenhang nachzudenken. Diese Perspektive kann zu einer bescheideneren Sicht unserer Stellung im Universum anregen.
