---
title: "Internet Sacred Text Archive"
description: "www.sacred-texts.com | ISTA - Internet Sacred Text Archive"
lead: ""
author: "John B. Hare"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-website.jpg"]
link: "https://www.sacred-texts.com/"
menu:
  resources:
    parent: "browse"
weight: 310
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Website"]
topics: ["Archive", "Mythology", "Religion", "The Tradition"]
---

The Internet Sacred Text Archive (ISTA) is a digital library that collects and makes available a wide range of religious and spiritual texts from a variety of traditions and cultures. The aim of the ISTA is to provide a comprehensive and easily accessible resource for those interested in exploring the religious and spiritual beliefs of different cultures and traditions. The ISTA features texts from ancient civilizations such as Egypt, Mesopotamia, Greece, and Rome, as well as texts from more recent traditions such as Buddhism, Hinduism, Judaism, Islam, and Christianity. Some of the most well-known texts available on the ISTA include the Hindu Vedas, the Buddhist Tripitaka, the Taoist Tao Te Ching, and the Christian Bible. The ISTA is an ongoing project and continues to expand its collection of sacred texts to provide a more comprehensive resource for those interested in exploring the spiritual and religious beliefs of humanity.

### Description

The following excerpt was directly taken from John B. Hare's website (from his [FAQ](https://www.sacred-texts.com/faq.htm)):

> This site strives to produce the best possible transcriptions of public domain texts on the subject of religion, mythology, folklore and the esoteric. The texts are posted for free access on the Internet. This site is like a public library: it is accessible to anyone, contains unfiltered information, and does not advocate any particular point of view. However, nobody is going to shush you if you make too much noise while using this site.

### See also

[Wiki › Religion]({{< relref "wiki/encyclopedia/religion.md" >}})</br>
