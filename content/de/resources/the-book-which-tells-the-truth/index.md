---
title: "The Book Which Tells The Truth"
description: "Claude Vorhilon aka Raël — The Book Which Tells The Truth (1973)"
lead: "The first book by Raël, the written testimony of his first several encounters with Yahweh."
author: "Claude Vorhilon (aka Raël)"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://wheelofheaven.github.io/rael-one-the-book-which-tells-the-truth/"
menu:
  resources:
    parent: "browse"
weight: 210
toc: false
pinned: true
featured: false
contributors: [""]
types: ["Maintained Books"]
topics: ["Ancient Astronauts", "Canon", "Elohim", "Intelligent Design", "Neo-Euhemerism"]
---

"The Book Which Tells the Truth" is a book written by Claude Vorilhon, also known as Raël, the founder of the Raëlian Movement. The book details Raël's claims of encounters with extraterrestrial beings and presents his beliefs and teachings.

According to Raël, the extraterrestrial beings he encountered revealed to him that life on Earth was created through genetic engineering by a highly advanced civilization of extraterrestrial beings, who he refers to as the Elohim. Raël claims that the Elohim created humans in their own image, and that they continue to observe and guide humanity's development.

The book presents Raël's beliefs about the nature of reality and the purpose of life. He argues that humans have a divine origin, and that our purpose is to evolve and eventually become like the Elohim, through the development of science and technology. Raël also teaches that the Elohim will return one day to bring about a new era of peace and enlightenment for humanity.

"The Book Which Tells the Truth" has been widely criticized for its claims of encounters with extraterrestrial beings, and for its teachings about the nature of reality and the purpose of life. The Raëlian Movement, which is based on the teachings presented in the book, has also been the subject of controversy, with some people claiming that it is a cult that is harmful to its members.

Despite these criticisms, the Raëlian Movement has continued to attract followers and has grown into a global organization with members in many countries. The book remains an important part of the Raëlian philosophy, and is considered by Raëlians to be a key source of knowledge and inspiration.

Overall, "The Book Which Tells the Truth" is a unique and controversial work that presents a highly unconventional view of the world and of human existence. Whether or not its claims and teachings are accepted by the wider public, the book remains an important expression of one person's vision of reality and of humanity's place in the universe.

![Image](images/le-message-book.jpg "Extraterrestrials Took Me To Their Planet, 1976 — Raël")

### About Claude Vorhilon

Claude Vorilhon, who goes by the name Raël, is a French spiritual leader and founder of the Raelian movement. Born in 1946, Raël worked as a race car driver, journalist, and singer before claiming to have a spiritual encounter with extraterrestrial beings in 1973.

According to Raël, these beings instructed him to spread their message of peace and love to humanity and to establish an embassy for them on Earth. He founded the Raelian movement in 1974, and it has since grown to become a global organization with thousands of members around the world.

The Raëlian movement's teachings draw on a mix of science, religion, and philosophy, and emphasize the idea that humans are descended from extraterrestrial beings

### Read it for yourself

📖 [Rael I — The Book Which Tells The Truth](https://wheelofheaven.github.io/rael-one-the-book-which-tells-the-truth/)

### See also

[Resources › Extraterrestrials Took Me To Their Planet]({{< relref "resources/extraterrestrials-took-me-to-their-planet/index.md" >}})</br>
[Wiki › Raëlism]({{< relref "wiki/encyclopedia/raelism.md" >}})</br>

### External links

[Download The Message | rael.org](https://www.rael.org/downloads/)
