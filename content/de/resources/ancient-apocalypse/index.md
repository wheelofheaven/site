---
title: "Ancient Apocalypse"
description: "Ancient Apocalypse: A Netflix production featuring Graham Hancock and his research."
lead: "A Netflix production featuring Graham Hancock and his research."
author: "Graham Hancock"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://www.netflix.com/ch-en/title/81211003"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Documentaries"]
topics: ["Age of Aquarius", "Builder Race", "Neo-Euhemerism", "Precession", "Pyramids", "The Tradition"]
---

### Synopsis

> The author argues that there is evidence for this lost civilization in the form of advanced ancient technologies and sophisticated astronomical knowledge that can be found in the ruins of several ancient cultures around the world, including the Mayans, the Egyptians, and the Incas. He suggests that this lost civilization was the source of much of the knowledge and wisdom that has been passed down to later civilizations, and that its influence can be seen in the architectural achievements and astronomical alignments of ancient structures such as the pyramids of Egypt and the temples of the Mayans.
>
> Hancock also explores the idea that this lost civilization was destroyed by a natural disaster, such as a comet impact, and that its survivors may have spread out across the world, taking their knowledge and wisdom with them. He suggests that the myths and legends of cultures around the world, such as the story of the Great Flood, may be based on the memories of this disaster and its aftermath.
>
> "Fingerprints of the Gods" is a highly speculative book that draws on evidence from a wide range of sources, including archaeology, anthropology, geology, and astronomy, to support its central argument. The book has been both praised and criticized for its imaginative and controversial ideas, and has sparked ongoing debate and discussion among scholars, researchers, and the general public.
>
> Overall, "Fingerprints of the Gods" is a thought-provoking and entertaining read that presents an alternative view of human history and the development of civilization. Whether or not the author's ideas are ultimately accepted by the wider academic community, the book remains an important and influential work in the field of alternative history and archaeology.

### See also

- [Graham Hancock\'s Fingerprints of the Gods\: The Evidence of Earth\'s Lost Civilization]({{< relref "resources/fingerprints-of-the-gods/index.md" >}})

### External links

- [Ancient Apocalypse | Wikipedia](https://en.wikipedia.org/wiki/Ancient_Apocalypse)
