---
title: "La Bibbia non è un libro sacro"
description: "Mauro Biglino — La Bibbia non è un libro sacro. Il grande inganno. 2013"
lead: "La divinità spiritualmente intesa non è presente nell’Antico Testamento. In particolare nella Bibbia non c’è Dio e non c’è culto rivolto a Dio. Ecco perché il titolo afferma che la Bibbia non è un Libro Sacro."
author: "Mauro Biglino"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://www.maurobiglino.com/libri/"
menu:
  resources:
    parent: "browse"
weight: 310
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Ancient Astronauts", "Elohim", "Neo-Euhemerism"]
---

"La Bibbia non è un libro sacro" (The Bible is Not a Sacred Book) is a book by Italian scholar Mauro Biglino, which challenges the traditional understanding of the Bible. In the book, Biglino argues that the texts of the Old Testament, specifically the Torah, were not divinely inspired, but rather were written by ancient scribes who recorded the history and beliefs of their society. He claims that the text has been mistranslated and misinterpreted over time, and that a more accurate understanding of the Bible would reveal that it is not a divine text, but rather a historical document that reflects the cultural and political context of its authors.

Biglino's work has been controversial and his claims have been met with criticism and opposition by many in the religious community. While some have welcomed his perspectives and approach, others have accused him of promoting a secular and secularist agenda. Despite the debate surrounding his work, "La Bibbia non è un libro sacro" has been widely read and discussed, and has had a significant impact on the way some people view and understand the Bible.

![Image](images/la-bibbia-non-e-un-libro-sacro-book.jpg "La Bibbia non è un libro sacro, 2013 — Mauro Biglino")

### About Mauro Biglino

Mauro Biglino is an Italian author and translator who is known for his controversial work on ancient texts and the Bible. He has translated several ancient texts from Hebrew and Aramaic into Italian, including the Old Testament and the Dead Sea Scrolls.

Biglino's translations and interpretations of the Bible have been controversial, as he argues that the text is not a religious or spiritual document, but rather a historical and cultural one. He believes that many of the stories and concepts in the Bible are based on earlier Mesopotamian and Sumerian myths and legends, and that they have been misinterpreted and distorted over time.

Biglino has also argued that the Bible contains references to advanced technology and extraterrestrial beings, and that these have been deliberately suppressed or obscured by religious authorities over the centuries. He has written several books on these topics, including "The Book That Will Forever Change Our Ideas About the Bible" and "The Earth Does Not Belong to Man."

Biglino's work has been met with both praise and criticism, with some scholars and readers finding his ideas thought-provoking and challenging, while others have dismissed his theories as unsubstantiated and far-fetched. Nonetheless, his work has contributed to the ongoing debate and discussion surrounding the origins and meaning of the Bible and other ancient texts.
