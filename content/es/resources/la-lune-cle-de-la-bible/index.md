---
title: "La Lune Clé de la Bible"
description: "Jean Sendy — La Lune Clé de la Bible (1968)"
lead: "Les fils d'Elohim s'aperçurent que les filles des hommes étaient belles. Ils prirent donc pour eux des femmes parmi toutes celles qu'ils avait élues... Quand elles enfantaient d'eux, c'étaient les héros qui furent jadis des hommes en renom. Genèse VI. 2 à 4. Les Bibles usuelles traduisent Elohim par Dieu: or, Elohim est un pluriel qui ne signifie nullement Dieux mais, au plus proche, les Anges. Quels sont donc ces Anges qui épousent les filles des hommes? Jean Sendy, en suivant pas à pas le texte de l'Ancient Testament, nous montre qu'il ne s'agit pas d'un récit légendaire, mettant en scène un Dieu unique et tout-puissant, mais d'un texte historique racontant la colonisation de la Terre par des Anges venus du ciel. Des Anges qui, à notre époque de fusée lunaire et de cosmonautes, deviennent beaucou[ plus vraisemblables et compréhensibles. Et la Bible redevient alors le prodigieux livre d'histoire qu'elle n'aurait jamais dû cesser d'être. Pour finir, Jean Sendy nous propose une preuve expérimentale de son extraordinaire hypothèse: si les Célèstes ont bien colonisé la Terre au temps de Moïse, des traces de leurs bases nous attendent sur la Luna, qui sera alors la 'clé de la Bible.'"
author: "Jean Sendy"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: ""
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Ancient Astronauts", "Elohim", "Neo-Euhemerism", "Precession"]
---

![Image](images/la-lune-cle-de-la-bible-book.jpg "La Lune Clé de la Bible 1968 — Jean Sendy")

### About Jean Sendy

Jean Sendy was a French author and esoteric writer who lived from 1910 to 1978. He is best known for his work on ancient astronauts and extraterrestrial intervention in human history. In his books, such as "Those Gods Who Made Heaven and Earth" and "The Coming of the Gods," Sendy explored the idea that human civilization was influenced or even created by beings from other planets.

Sendy's work drew on a range of sources, including mythology, ancient texts, and scientific theories, to develop his ideas. He argued that many ancient cultures and religions had similar myths and legends about gods or divine beings who came from the sky, and that these stories could be interpreted as evidence of extraterrestrial contact.

While Sendy's ideas were not widely accepted by mainstream scholars, his work had a significant impact on popular culture, and influenced later writers and researchers in the fields of ufology, ancient astronaut theory, and the search for extraterrestrial life.

### See also

[Wiki › Neo-Euhemerism]({{< relref "wiki/encyclopedia/neo-euhemerism.md" >}})</br>
[Resources › The Coming Of The Gods by Jean Sendy]({{< relref "resources/the-coming-of-the-gods/_index.md" >}})</br>
[Resources › Those Gods Who Made Heaven and Earth by Jean Sendy]({{< relref "resources/those-gods-who-made-heaven-and-earth/_index.md" >}})</br>

### External links

[La Lune Clé de la Bible](https://books.google.ch/books/about/La_lune_cl%C3%A9_de_la_bible.html?id=Q8ECrgEACAAJ)</br>
