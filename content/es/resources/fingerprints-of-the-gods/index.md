---
title: "Fingerprints of the Gods: The Evidence of Earth's Lost Civilization"
description: "Graham Hancock — Fingerprints of the Gods: The Evidence of Earth's Lost Civilization (1995)"
lead: "Travelling first to South and Meso-America, Graham finds evidence of myths of a white-skinned ‘god’ named Quetzalcoatl or ‘Viracocha’ who came from a drowned land bringing knowledge of farming and culture after a great flood. Tied in with these myths Graham begins to crack an ancient code imprinted in these ancient tales that refer to the ‘great mill’ of the heavens.

It is an astronomical code that deals with the position of the stars over vast periods of time – a code that reveals the ancients knew far, far more than they are generally credited with. Traces of the same code appear in Egyptian myth, and it is to this desert land that Graham and Santha travel, finding there haunting parallels in architecture and ritual to the New World sites they have just left behind.

Moreover, the whole layout of the Giza plateau seems to point to a date many thousands of years earlier than the date of its supposed construction – a date revealed in the astronomical alignments of the Pyramids, the ‘mansions of a million years’, home of the god Osiris, the bringer of agriculture to the Egyptians, like Quetzalcoatl, after a flood."
author: "Graham Hancock"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-gitbook.jpg"]
link: "https://grahamhancock.com/fingerprints/"
menu:
  resources:
    parent: "browse"
weight: 240
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Age of Aquarius", "Builder Race", "Neo-Euhemerism", "Precession", "Pyramids", "The Tradition"]
---

"Fingerprints of the Gods: The Evidence of Earth's Lost Civilization" is a book written by Graham Hancock that proposes the existence of a lost civilization that existed thousands of years ago and has had a profound impact on the development of human culture and civilization.

The author argues that there is evidence for this lost civilization in the form of advanced ancient technologies and sophisticated astronomical knowledge that can be found in the ruins of several ancient cultures around the world, including the Mayans, the Egyptians, and the Incas. He suggests that this lost civilization was the source of much of the knowledge and wisdom that has been passed down to later civilizations, and that its influence can be seen in the architectural achievements and astronomical alignments of ancient structures such as the pyramids of Egypt and the temples of the Mayans.

Hancock also explores the idea that this lost civilization was destroyed by a natural disaster, such as a comet impact, and that its survivors may have spread out across the world, taking their knowledge and wisdom with them. He suggests that the myths and legends of cultures around the world, such as the story of the Great Flood, may be based on the memories of this disaster and its aftermath.

"Fingerprints of the Gods" is a highly speculative book that draws on evidence from a wide range of sources, including archaeology, anthropology, geology, and astronomy, to support its central argument. The book has been both praised and criticized for its imaginative and controversial ideas, and has sparked ongoing debate and discussion among scholars, researchers, and the general public.

Overall, "Fingerprints of the Gods" is a thought-provoking and entertaining read that presents an alternative view of human history and the development of civilization. Whether or not the author's ideas are ultimately accepted by the wider academic community, the book remains an important and influential work in the field of alternative history and archaeology.

### About Graham Hancock

Graham Hancock is a British writer, journalist, and researcher who is best known for his work on ancient civilizations, prehistoric monuments, and altered states of consciousness. He has authored several books, including the international bestsellers "Fingerprints of the Gods," "The Sign and the Seal," and "Magicians of the Gods," in which he presents his theories and research on various historical and archaeological mysteries.

Hancock's work often explores unconventional theories and ideas, including the possibility of lost civilizations, extraterrestrial intervention in human history, and the use of psychoactive substances in ancient religious practices. He has been a controversial figure in some academic circles, as his theories and interpretations have been met with skepticism by some experts in the fields of archaeology, anthropology, and history.

### See also

[Wiki › Neo-Euhemerism]({{< relref "wiki/encyclopedia/neo-euhemerism.md" >}})</br>
[Resources › L\'Ère du Verseau: Fin de l\'Illusion Humaniste by Jean Sendy \[FR\]]({{< relref "resources/l-ere-du-verseau/_index.md" >}})</br>
[Resources › Those Gods Who Made Heaven and Earth by Jean Sendy]({{< relref "resources/those-gods-who-made-heaven-and-earth/_index.md" >}})</br>

### External links

[Fingerprint of the Gods | Wikipedia](https://en.wikipedia.org/wiki/Fingerprints_of_the_Gods)</br>
[The Coming Of The Gods | Google Books](https://books.google.ch/books?id=nlVlAAAACAAJ&sitesec=reviews&redir_esc=y)</br>
[The Coming Of The Gods | GoodReads](https://www.goodreads.com/book/show/4977839-the-coming-of-the-gods)</br>
