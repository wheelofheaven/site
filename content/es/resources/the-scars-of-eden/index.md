---
title: "The Scars of Eden: Has humanity confused the idea of God with memories of ET contact?"
description: "Paul Anthony Wallis — The Scars of Eden: Has Humanity confused the idea of God with memories of ET contact? (2021)"
lead: "Do our world mythologies convey our ancestors' ideas about God? Or are they in reality ancestral memories of extra-terrestrial contact? How do ancient stories of contact, adaptation and abduction relate to people's experiences around the world today? The Scars of Eden will take you around the world to hear first-hand from ancestral voices alongside contemporary experiencers and world-renowned researchers. Recent revelations from US Navy, the Pentagon, and French Intelligence bring the reader right up to date in examining what has been forgotten and remembered, hidden and disclosed. If world mythologies, including the Bible, have confused the idea of God with ancient ET visitations, what difference does it make? How does it impact society today? And why is this cultural taboo so widespread and, for the author, so personal?"
author: "Paul Anthony Wallis"
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: ["resources-book.jpg"]
link: "https://paulanthonywallis.com/2020/07/30/the-scars-of-eden/"
menu:
  resources:
    parent: "browse"
weight: 315
toc: false
pinned: false
featured: false
contributors: [""]
types: ["Books"]
topics: ["Ancient Astronauts", "Elohim", "Neo-Euhemerism"]
---

"The Scars of Eden: Has humanity confused the idea of God with memories of ET contact?" is a book written by Paul Anthony Wallis, an author and researcher in the fields of religion and extraterrestrial life. The book presents the author's views on the relationship between humanity's understanding of God and extraterrestrial life.

The book argues that humanity's belief in God and religious practices may have their roots in ancient experiences of extraterrestrial contact. Wallis asserts that many religious beliefs, including the creation stories found in various religions, may be based on actual events of extraterrestrial visitations that have been misinterpreted and mythologized over time.

The author uses evidence from archaeology, anthropology, and religious texts to support his argument that extraterrestrial visitations have had a profound influence on human culture and religion. He also examines the similarities between religious beliefs and accounts of extraterrestrial encounters, suggesting that these experiences may have shaped humanity's understanding of God and the afterlife.

"The Scars of Eden" has been met with criticism from some religious scholars and mainstream scientists, who reject the author's claims as unsupported and lacking credible evidence. Nevertheless, the book remains a popular and widely discussed work in the field of alternative religion and extraterrestrial life.

![Image](images/the-scars-of-eden-book.jpg "The Scars of Eden, 2021 — Paul Anthony Wallis")

### About Paul Anthony Wallis

Paul Anthony Wallis is an author, researcher, and speaker, who has written several books on the topics of early Christianity, Gnosticism, and the origins of humanity. He is best known for his books "Scars of Eden" and "Echoes of Eden," which explore the possibility that the biblical story of Adam and Eve is based on ancient accounts of humanity's origins and interactions with extraterrestrial beings.

Wallis has a background in theology, history, and archaeology, and has spent over two decades researching the origins of the Judeo-Christian tradition. He has also studied Gnosticism, a set of religious and philosophical beliefs that emerged in the early Christian era and were considered heretical by the Church.

In addition to his writing, Wallis has given presentations on his research at conferences and events around the world. He has also appeared on various television programs and podcasts to discuss his work.
