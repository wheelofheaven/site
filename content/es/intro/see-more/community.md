---
title: "Comunidad"
description: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
excerpt: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 420
toc: true
---

Si las premisas de Wheel of Heaven despertaron tu curiosidad, también puedes participar para que la experiencia sea más valiosa ayudándonos a agregar piezas de información faltantes o a traducir contenido no disponible a uno de los idiomas admitidos.

También puedes conectarte con nosotros a través de Telegram, Github y/o Twitter. En el encabezado, encontrarás los enlaces a cada una de estas plataformas haciendo clic en el icono correspondiente.

La forma más directa de contactarnos es por correo electrónico. Obtén la dirección de correo electrónico oficial actual en nuestra página de [Contacto]({{< ref "/contact" >}}).
