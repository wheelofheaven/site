---
title: "Base de conocimientos"
description: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
excerpt: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "see-more"
weight: 410
toc: true
---

Has llegado a la Introducción, pero el viaje inquisitivo no termina aquí. Asegúrate de revisar la Wiki, los Recursos y la sección de Blog de Wheel of Heaven para sumergirte más en la gran narrativa presentada aquí.

- [Una sección informativa de la Wiki llena de entradas y puntos de interés 🔗]({{< ref "/wiki/" >}})
- [Un área de recursos que presenta libros web seleccionados por nuestro equipo 🔗]({{< ref "/resources/" >}})
- [Un blog actualizado con frecuencia donde se comparten perspectivas frescas y conocimientos 🔗]({{< ref "/articles/" >}})
