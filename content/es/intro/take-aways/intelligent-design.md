---
title: "Diseño inteligente"
description: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
excerpt: "Wheel of Heaven es una base de conocimientos que explora la hipótesis de trabajo de que la vida en la Tierra fue diseñada inteligentemente por una civilización extraterrestre, los llamados Elohim."
date: 2022-09-20T00:00:00+00:00
lastmod: 2022-09-20T00:00:00+00:00
draft: false
images: []
menu:
  intro:
    parent: "take-aways"
weight: 320
toc: true
---

La explicación científica ampliamente aceptada para los orígenes de la vida en la Tierra es la teoría de la abiogénesis, que sugiere que la vida emergió de compuestos orgánicos simples a través de una serie de procesos químicos naturales.

La idea de que seres extraterrestres crearon la vida en la Tierra es ciertamente una desviación de las explicaciones científicas y religiosas tradicionales.

Aunque generalmente se consideraría una creencia marginal o incluso pseudocientífica, el concepto de Diseño Inteligente de Raëlismo plantea preguntas provocativas sobre qué constituye la vida y la inteligencia, y el papel que estos factores juegan en el desarrollo de las especies.
